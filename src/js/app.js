import sayHello from './lib/sayHello.js';
import $ from 'jquery';

sayHello();

$(document).ready(function() {
  console.log('ready');
  $('.alert-btn').on('mouseover', function(e) {
    console.log('mouseover', e);
    $(this).prepend('<div class="alert-btn__bubble"></div>');
    var bubble = $(this).find('.alert-btn__bubble');
    bubble.css({top:e.clientY-this.getBoundingClientRect().top, left: e.clientX-this.getBoundingClientRect().left });
  });
  $('.alert-btn').on('mouseout', function(e) {
    console.log('mouseout', e);
    $(this).find('.alert-btn__bubble').remove();
  });
  $('.alert-btn').on('mousemove', function(e) {
    console.log('mousemove', e.clientX, e.clientY);
    console.log('mousemove', this.getBoundingClientRect());
    console.log('mousemove', e.clientX-this.getBoundingClientRect().left, e.clientY-this.getBoundingClientRect().top);
    // this.append('<div class="alert-btn__bubble"></div>');
    var bubble = $(this).find('.alert-btn__bubble');
    bubble.css({top:e.clientY-this.getBoundingClientRect().top, left: e.clientX-this.getBoundingClientRect().left });
  });
});

function burgerMenu(selector) {
  let menu = $(selector);

  let body = $('body');
  let nav = $('.burger-menu_nav');

  let button = menu.find('.burger-menu_button', '.burger-menu_lines');
  let links = menu.find('.burger-menu_link');
  let overlay = menu.find('.burger-menu_overlay');
  
  console.log('nav', nav);
  button.on('click', (e) => {
    e.preventDefault();
    toggleMenu();
  });
  
  links.on('click', () => toggleMenu());
  overlay.on('click', () => toggleMenu());
  
  function toggleMenu() {
    menu.toggleClass('burger-menu_active');
    nav.toggleClass('burger-menu_active');
    body.toggleClass('burger-menu_active');
    
    // if (menu.hasClass('burger-menu_active')) {
    //   $('body').css('overlow', 'hidden');
    // } else {
    //   $('body').css('overlow', 'visible');
    // }
  }
}

burgerMenu('.burger-menu');
